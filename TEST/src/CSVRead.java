import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class CSVRead {

	private static String filename = "D:/01.solrip0722/01.eclipse/01.workspace/TEST/data/title.csv";
	private static String body = "D:/01.solrip0722/01.eclipse/01.workspace/TEST/data/body.csv";

	public static void main(String[] args) {
		readTitle(body, "euc-kr");
	}

	private static void readTitle(String path, String encoding) {
		BufferedReader br = null;
		String line;
		String cvsSplitBy = "\\^";

		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(path), encoding));
			while ((line = br.readLine()) != null) {
				String[] field = line.split(cvsSplitBy);
				System.out.println(field[1]);
//				 break;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
