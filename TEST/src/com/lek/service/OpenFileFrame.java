package com.lek.service;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JButton;

public class OpenFileFrame extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OpenFileFrame frame = new OpenFileFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OpenFileFrame() {
		setTitle("파일 업로드");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 322, 137);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JComboBox combo_Business = new JComboBox();
		combo_Business.setBounds(62, 25, 227, 21);
		contentPane.add(combo_Business);
		
		JLabel lb_Business = new JLabel("사업명");
		lb_Business.setBounds(12, 28, 42, 15);
		contentPane.add(lb_Business);
		
		JButton btn_FileOpen = new JButton("파일등록 및 저장");
		btn_FileOpen.setBounds(62, 56, 227, 23);
		contentPane.add(btn_FileOpen);
	}
}
