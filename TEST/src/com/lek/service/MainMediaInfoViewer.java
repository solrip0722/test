package com.lek.service;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.Font;


public class MainMediaInfoViewer extends JFrame {

	private JPanel contentPane;

	private JTable tb_View;
	public JTextField tf_Title;
	public JTextField tf_Address;
	public JTextField tf_Gather;
	public JTextField tf_Event;
	public JTextField tf_Search;
	public JTextField tf_ItemID;
	public JTextArea ta_Memo;

	public JCheckBox cb_Impossible;
	public JCheckBox cb_Groundshot;
	public JCheckBox cb_Reshoot;
	public JCheckBox cb_Is_suc;

	public JComboBox combo_Address;
	public JComboBox combo_Impossible;
	public JComboBox combo_Ground;
	public JComboBox combo_Aerial;
	public JComboBox combo_Reshoot;
	public JComboBox combo_Gather;
	public JComboBox combo_Event;
	public JComboBox combo_IsSuc;

	public DefaultTableModel model;
	
	private JMenuItem menuItem_Save;
	private JMenuItem menuItem_SaveAs;
	private JMenuItem menuItem_Exit;
	private JMenuItem menuItem_Print;
	private JComboBox combo_Business;
	private JButton btn_searchDel;
	private JTextField tf_Aerial;

	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMediaInfoViewer frame = new MainMediaInfoViewer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public MainMediaInfoViewer() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1600, 1000);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu topMenu = new JMenu("메뉴");
		menuBar.add(topMenu);

		JMenuItem menuItem_Insert = new JMenuItem("사업등록");
		//topMemuInsert.add(new BusinessRegFrameView());
		menuItem_Insert.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_MASK));
		topMenu.add(menuItem_Insert);
		
		JMenuItem menuItem_Open = new JMenuItem("열기");
		topMenu.add(menuItem_Open);
		menuItem_Open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_MASK));
		
		menuItem_Save = new JMenuItem("저장");
		topMenu.add(menuItem_Save);
		menuItem_Save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK));
		
		menuItem_SaveAs = new JMenuItem("다른이름으로 저장");
		topMenu.add(menuItem_SaveAs);
		topMenu.addSeparator();
		
		menuItem_Print = new JMenuItem("인쇄");
		menuItem_Print.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_MASK));
		topMenu.add(menuItem_Print);
		
		
		topMenu.addSeparator();
		menuItem_Exit = new JMenuItem("종료");
		topMenu.add(menuItem_Exit);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		tf_Title = new JTextField();
		tf_Title.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tf_Title.setBounds(56, 50, 170, 21);
		tf_Title.setToolTipText("\uD56D\uBAA9\uBA85");
		tf_Title.setColumns(10);

		cb_Reshoot = new JCheckBox("\uC7AC\uCD2C\uC601");
		cb_Reshoot.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		cb_Reshoot.setBounds(639, 81, 80, 23);

		tf_Event = new JTextField();
		tf_Event.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tf_Event.setBounds(56, 82, 170, 21);
		tf_Event.setToolTipText("\uD589\uC0AC");
		tf_Event.setColumns(10);
		contentPane.setLayout(null);

		String title[]= {"\uC21C\uC11C", "\uD56D\uBAA9\uBA85", "\uD56D\uBAA9 ID", "\uC18C\uC7AC\uC9C0", "\uBD88\uAC00\uB2A5 \uD56D\uBAA9", "\uC9C0\uC0C1\uCD2C\uC601", "\uD56D\uACF5\uCD2C\uC601", "\uC218\uC9D1", "\uD589\uC0AC", "\uC7AC\uCD2C\uC601", "\uC644\uB8CC\uC5EC\uBD80", "\uBA54\uBAA8", "\uB4F1\uB85D\uC77C", "\uC218\uC815\uC77C"};
		model = new DefaultTableModel(title, 0);
		
				JLabel lb_Filter = new JLabel("필터");
				lb_Filter.setFont(new Font("맑은 고딕", Font.BOLD, 12));
				lb_Filter.setBackground(Color.BLACK);
				lb_Filter.setBounds(223, 10, 1359, 21);
				contentPane.add(lb_Filter);

		JLabel lb_Title = new JLabel("\uD56D\uBAA9\uBA85");
		lb_Title.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		lb_Title.setBounds(12, 53, 42, 15);
		contentPane.add(lb_Title);

		JLabel lb_Event = new JLabel("\uD589\uC0AC");
		lb_Event.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		lb_Event.setBounds(24, 85, 30, 15);
		contentPane.add(lb_Event);

		JLabel lb_Address = new JLabel("\uC18C\uC7AC\uC9C0");
		lb_Address.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		lb_Address.setBounds(444, 53, 42, 15);
		contentPane.add(lb_Address);

		JLabel lb_Gather = new JLabel("\uC218\uC9D1");
		lb_Gather.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		lb_Gather.setBounds(665, 53, 31, 15);
		contentPane.add(lb_Gather);

		JLabel lb_Memo = new JLabel("\uBA54\uBAA8");
		lb_Memo.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		lb_Memo.setBounds(825, 53, 30, 15);
		contentPane.add(lb_Memo);

		JLabel lb_ItemID = new JLabel("   항목ID");
		lb_ItemID.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		lb_ItemID.setBounds(232, 53, 56, 15);
		contentPane.add(lb_ItemID);
		
		JLabel lb_Search = new JLabel("검색");
		lb_Search.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		lb_Search.setBounds(1142, 53, 30, 15);
		contentPane.add(lb_Search);
		
		JLabel lb_Business = new JLabel("사업명");
		lb_Business.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		lb_Business.setBounds(12, 13, 42, 15);
		contentPane.add(lb_Business);
		contentPane.add(tf_Title);
		contentPane.add(tf_Event);

		tf_ItemID = new JTextField();
		tf_ItemID.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tf_ItemID.setToolTipText("\uC18C\uC7AC\uC9C0");
		tf_ItemID.setColumns(10);
		tf_ItemID.setBounds(285, 50, 150, 21);
		contentPane.add(tf_ItemID);

		tf_Gather = new JTextField();
		tf_Gather.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tf_Gather.setBounds(692, 50, 116, 21);
		tf_Gather.setToolTipText("\uC218\uC9D1");
		tf_Gather.setColumns(10);
		contentPane.add(tf_Gather);

		tf_Address = new JTextField();
		tf_Address.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tf_Address.setBounds(488, 50, 170, 21);
		tf_Address.setToolTipText("\uC18C\uC7AC\uC9C0");
		tf_Address.setColumns(10);
		contentPane.add(tf_Address);

		JScrollPane ta_ScrollPane = new JScrollPane();
		ta_ScrollPane.setBounds(852, 50, 266, 50);

		ta_Memo = new JTextArea();
		ta_Memo.setLineWrap(true);
		ta_ScrollPane.setViewportView(ta_Memo);
		contentPane.add(ta_ScrollPane);

		cb_Impossible = new JCheckBox("\uBD88\uAC00\uB2A5 \uD56D\uBAA9");
		cb_Impossible.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		cb_Impossible.setBounds(442, 81, 101, 23);
		contentPane.add(cb_Impossible);

		cb_Groundshot = new JCheckBox("\uC9C0\uC0C1\uCD2C\uC601");
		cb_Groundshot.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		cb_Groundshot.setBounds(551, 81, 80, 23);
		contentPane.add(cb_Groundshot);
		contentPane.add(cb_Reshoot);

		cb_Is_suc = new JCheckBox("\uC644\uB8CC \uC5EC\uBD80");
		cb_Is_suc.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		cb_Is_suc.setBounds(727, 81, 80, 23);
		contentPane.add(cb_Is_suc);

		tf_Search = new JTextField();
		tf_Search.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tf_Search.setToolTipText("항목 검색");
		tf_Search.setColumns(10);
		tf_Search.setBounds(1170, 49, 180, 23);
		contentPane.add(tf_Search);
		
		JButton btn_Insert = new JButton("\uB4F1\uB85D");
		btn_Insert.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		btn_Insert.setBounds(1361, 79, 101, 23);
		contentPane.add(btn_Insert);

		JButton btn_Search = new JButton("\uAC80\uC0C9");
		btn_Search.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		btn_Search.setBounds(1361, 49, 101, 23);
		contentPane.add(btn_Search);

		JButton btn_Clear = new JButton("\uAC12 \uCD08\uAE30\uD654");
		btn_Clear.setFont(new Font("맑은 고딕", Font.BOLD, 12));
//		btn_Clear.addActionListener(new ClearEventActionListener(this));
		btn_Clear.setBounds(1476, 79, 101, 23);
		contentPane.add(btn_Clear);

		JButton btn_DeleteAll = new JButton("\uC804\uCCB4 \uC0AD\uC81C");
		btn_DeleteAll.setFont(new Font("맑은 고딕", Font.BOLD, 12));
//		btn_DeleteAll.addActionListener(new DBDeleteEventActionListener());
		btn_DeleteAll.setVerticalAlignment(SwingConstants.BOTTOM);
		btn_DeleteAll.setBackground(new Color(255, 0, 0));
		btn_DeleteAll.setForeground(Color.WHITE);
		btn_DeleteAll.setBounds(1476, 9, 101, 23);
		contentPane.add(btn_DeleteAll);

		JButton btn_Reload = new JButton("\uC0C8\uB85C\uACE0\uCE68");
		btn_Reload.setFont(new Font("맑은 고딕", Font.BOLD, 12));
//		btn_Reload.addActionListener(new ReLoadEventActionListener(this));
		btn_Reload.setBounds(1476, 49, 101, 23);
		contentPane.add(btn_Reload);
		
				JButton btn_FilterSearch = new JButton("필터검색");
				btn_FilterSearch.setFont(new Font("맑은 고딕", Font.BOLD, 12));
				//		btn_FilterSearch.addActionListener(new FilterSearchEventActionListener(this));
						btn_FilterSearch.setBounds(1360, 9, 101, 23);
						contentPane.add(btn_FilterSearch);
		
		btn_searchDel = new JButton("검색삭제");
		btn_searchDel.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		btn_searchDel.setToolTipText("검색 대상 삭제");
		btn_searchDel.setBounds(1170, 81, 180, 23);
		contentPane.add(btn_searchDel);

		combo_Address = new JComboBox();
		combo_Address.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		combo_Address.setModel(new DefaultComboBoxModel(new String[] {"소재지", "오름차순", "내림차순"}));
		combo_Address.setName("combo_Address");
		combo_Address.setToolTipText("소재지");
		combo_Address.setBounds(250, 10, 120, 21);
		contentPane.add(combo_Address);

		combo_Impossible = new JComboBox();
		combo_Impossible.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		combo_Impossible.setName("combo_Impossible");
		combo_Impossible.setToolTipText("불가능 항목");
		combo_Impossible.setModel(new DefaultComboBoxModel(new String[] {"불가능 항목", "내용없음", "o", "x"}));
		combo_Impossible.setBounds(390, 10, 120, 21);
		contentPane.add(combo_Impossible);

		combo_Ground = new JComboBox();
		combo_Ground.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		combo_Ground.setName("combo_Ground");
		combo_Ground.setToolTipText("지상촬영");
		combo_Ground.setModel(new DefaultComboBoxModel(new String[] {"지상촬영", "내용없음", "필요없음", "o", "x"}));
		combo_Ground.setBounds(530, 10, 120, 21);
		contentPane.add(combo_Ground);

		combo_Aerial = new JComboBox();
		combo_Aerial.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		combo_Aerial.setName("combo_Aerial");
		combo_Aerial.setToolTipText("항공촬영");
		combo_Aerial.setModel(new DefaultComboBoxModel(new String[] {"항공촬영", "내용없음", "필요없음", "o", "x"}));
		combo_Aerial.setBounds(670, 10, 120, 21);
		contentPane.add(combo_Aerial);

		combo_Reshoot = new JComboBox();
		combo_Reshoot.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		combo_Reshoot.setName("combo_Reshoot");
		combo_Reshoot.setModel(new DefaultComboBoxModel(new String[] {"재촬영", "내용없음", "o", "x"}));
		combo_Reshoot.setToolTipText("재촬영");
		combo_Reshoot.setBounds(810, 10, 120, 21);
		contentPane.add(combo_Reshoot);

		combo_Gather = new JComboBox();
		combo_Gather.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		combo_Gather.setName("combo_Gather");
		combo_Gather.setModel(new DefaultComboBoxModel(new String[] {"수집", "내용없음", "필요없음", "o", "x"}));
		combo_Gather.setToolTipText("수집");
		combo_Gather.setBounds(950, 10, 120, 21);
		contentPane.add(combo_Gather);

		combo_Event = new JComboBox();
		combo_Event.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		combo_Event.setName("combo_Event");
		combo_Event.setModel(new DefaultComboBoxModel(new String[] {"행사", "내용없음", "o", "x"}));
		combo_Event.setToolTipText("행사");
		combo_Event.setBounds(1090, 10, 120, 21);
		contentPane.add(combo_Event);

		combo_IsSuc = new JComboBox();
		combo_IsSuc.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		combo_IsSuc.setName("combo_IsSuc");
		combo_IsSuc.setModel(new DefaultComboBoxModel(new String[] {"완료여부", "내용없음", "o", "x"}));
		combo_IsSuc.setToolTipText("완료여부");
		combo_IsSuc.setBounds(1230, 10, 120, 21);
		contentPane.add(combo_IsSuc);
		
		combo_Business = new JComboBox();
		combo_Business.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		combo_Business.setBounds(50, 10, 150, 21);
		contentPane.add(combo_Business);

		Canvas cv_Line = new Canvas();
		cv_Line.setEnabled(false);
		cv_Line.setBackground(new Color(0, 0, 0));
		cv_Line.setBounds(12, 39, 1570, 2);
		contentPane.add(cv_Line);

		JScrollPane tb_ScrollPane = new JScrollPane();
		tb_ScrollPane.setBounds(12, 109, 1570, 842);
		contentPane.add(tb_ScrollPane);

		tb_View = new JTable();
		tb_View.setModel(model);
		tb_View.getColumnModel().getColumn(0).setPreferredWidth(15);
		tb_View.getColumnModel().getColumn(1).setPreferredWidth(165);
		tb_View.getColumnModel().getColumn(3).setPreferredWidth(50);
		tb_View.getColumnModel().getColumn(4).setPreferredWidth(40);
		tb_View.getColumnModel().getColumn(5).setPreferredWidth(40);
		tb_View.getColumnModel().getColumn(6).setPreferredWidth(40);
		tb_View.getColumnModel().getColumn(7).setPreferredWidth(40);
		tb_View.getColumnModel().getColumn(8).setPreferredWidth(50);
		tb_View.getColumnModel().getColumn(9).setPreferredWidth(30);
		tb_View.getColumnModel().getColumn(10).setPreferredWidth(30);
		tb_View.getColumnModel().getColumn(11).setPreferredWidth(255);
		tb_View.getColumnModel().getColumn(12).setPreferredWidth(80);
		tb_View.getColumnModel().getColumn(13).setPreferredWidth(80);
		tb_ScrollPane.setViewportView(tb_View);
		
		tf_Aerial = new JTextField();
		tf_Aerial.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tf_Aerial.setToolTipText("소재지");
		tf_Aerial.setColumns(10);
		tf_Aerial.setBounds(285, 82, 150, 21);
		contentPane.add(tf_Aerial);
		
		JLabel lb_Aerial = new JLabel("항공촬영");
		lb_Aerial.setFont(new Font("맑은 고딕", Font.BOLD, 12));
		lb_Aerial.setBounds(232, 85, 56, 15);
		contentPane.add(lb_Aerial);
	}
}
