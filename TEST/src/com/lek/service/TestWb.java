package com.lek.service;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Canvas;

public class TestWb extends JFrame {

	public JPanel contentPane;
	public DefaultTableModel tb_ImageScanModel;
	private JTextField tf_Search;
	private JTextField tf_Partner;
	private JTextField tf_Business;
	private JTextField tf_EndTime;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestWb frame = new TestWb();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestWb() {
		setTitle("namoo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 614, 336);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JComboBox combo_filter = new JComboBox();
		combo_filter.setModel(new DefaultComboBoxModel(new String[] {"사업자", "사업자명"}));
		combo_filter.setBounds(12, 11, 220, 21);
		contentPane.add(combo_filter);
		
		tf_Search = new JTextField();
		tf_Search.setBounds(244, 11, 233, 21);
		contentPane.add(tf_Search);
		tf_Search.setColumns(10);
		
		tf_Partner = new JTextField();
		tf_Partner.setColumns(10);
		tf_Partner.setBounds(51, 42, 100, 21);
		contentPane.add(tf_Partner);
		
		tf_Business = new JTextField();
		tf_Business.setColumns(10);
		tf_Business.setBounds(212, 42, 113, 21);
		contentPane.add(tf_Business);
		
		tf_EndTime = new JTextField();
		tf_EndTime.setColumns(10);
		tf_EndTime.setBounds(377, 42, 100, 21);
		contentPane.add(tf_EndTime);
		
		JButton btn_Search = new JButton("검색");
		btn_Search.setBounds(489, 10, 97, 23);
		contentPane.add(btn_Search);
		
		JButton btn_Insert = new JButton("등록");
		btn_Insert.setBounds(489, 43, 97, 23);
		contentPane.add(btn_Insert);
		
		JLabel lb_Partner = new JLabel("사업자");
		lb_Partner.setBounds(12, 45, 40, 15);
		contentPane.add(lb_Partner);
		
		JLabel lb_Business = new JLabel("사업명");
		lb_Business.setBounds(169, 45, 40, 15);
		contentPane.add(lb_Business);
		
		JLabel lb_EndTime = new JLabel("종료일");
		lb_EndTime.setBounds(337, 45, 40, 15);
		contentPane.add(lb_EndTime);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 73, 574, 214);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"\uC21C\uC11C", "\uC0AC\uC5C5\uC790", "\uC0AC\uC5C5\uBA85", "\uB4F1\uB85D\uC77C", "\uC885\uB8CC\uC77C"
			}
		));
		table.getColumnModel().getColumn(0).setPreferredWidth(35);
		table.getColumnModel().getColumn(1).setPreferredWidth(102);
		table.getColumnModel().getColumn(2).setPreferredWidth(101);
		scrollPane.setViewportView(table);
		
		Canvas canvas = new Canvas();
		canvas.setBackground(Color.DARK_GRAY);
		canvas.setBounds(10, 36, 576, 1);
		contentPane.add(canvas);
	}
}
