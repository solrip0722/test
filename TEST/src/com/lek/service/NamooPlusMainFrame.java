package com.lek.service;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import java.awt.Canvas;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class NamooPlusMainFrame extends JFrame {

	public JPanel contentPane;

	public final ButtonGroup bgRS_Radio = new ButtonGroup();
	public final ButtonGroup bgPT_Radio = new ButtonGroup();
	
	public JTable tb_RouteScan;
	public JTable tb_ImageScan;
	
	public JTextField tfRS_Address;
	public JTextField tfRS_Title;
	public JTextField tfRS_Location;
	public JTextField tfGC_Address;
	public JTextField tfGC_GeoCode;
	public JTextField tfPT_PageNum;
	public JTextField tfPT_DocPw;
	public JTextField tfPT_Resolution;
	public JTextField tfPT_TotPageNum;
	public JTextField tfPT_HiddenFilePath;
	
	public DefaultTableModel tb_ImageScanModel;
	public DefaultTableModel tb_RouteScanModel;
	
	public JLabel lbRS_ImgIcon;
	public JLabel lbGC_ImgIcon;
	
	public JButton btnBT_Directory;
	public JButton btnBT_Files;
	public JButton btnBT_Save;
	public JButton btnBT_Clear;
	private JTextField tf_searchDelete;
	private JTextField tfRS_LimitCnt;
	private JTextField tfRS_Zoom;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NamooPlusMainFrame frame = new NamooPlusMainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NamooPlusMainFrame() {
		setTitle("namoo");
		setResizable(false); // 프레임 고정
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1250, 760);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		//tabbedPane.addChangeListener(new EventChangedListener(this));
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		JPanel imageScanPanel = new JPanel();
		tabbedPane.addTab("Image Scan", null, imageScanPanel, null);
		imageScanPanel.setLayout(new GridLayout(0, 1, 0, 0));

		JScrollPane scrollPane_ImageScan = new JScrollPane();
		imageScanPanel.add(scrollPane_ImageScan);

		tb_ImageScan = new JTable();
		tb_ImageScan.setCellSelectionEnabled(true);
		scrollPane_ImageScan.setViewportView(tb_ImageScan);
		tb_ImageScan.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tb_ImageScan.getTableHeader().setReorderingAllowed(false); // 컬럼 이동 불가
		//tb_ImageScanModel = new DefaultTableModel(ResourceData.VECTOR_JTABLE_IMAGE_SCAN_COLUMNS, 0);
		tb_ImageScan.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null},
			},
			new String[] {
				"\uC21C\uC11C", "\uD30C\uC77C\uBA85", "\uC704\uB3C4", "\uACBD\uB3C4", "\uCD2C\uC601\uC77C", "\uCD2C\uC601\uC2DC\uAC04", "\uC8FC\uC18C"
			}
		));
		tb_ImageScan.getColumnModel().getColumn(0).setPreferredWidth(41);
		tb_ImageScan.getColumnModel().getColumn(1).setPreferredWidth(263);
		tb_ImageScan.getColumnModel().getColumn(2).setPreferredWidth(150);
		tb_ImageScan.getColumnModel().getColumn(3).setPreferredWidth(150);
		tb_ImageScan.getColumnModel().getColumn(4).setPreferredWidth(150);
		tb_ImageScan.getColumnModel().getColumn(5).setPreferredWidth(150);
		tb_ImageScan.getColumnModel().getColumn(6).setPreferredWidth(318);

		JPanel routeScanPanel = new JPanel();
		tabbedPane.addTab("Route Scan", null, routeScanPanel, null);
		routeScanPanel.setLayout(null);

		JRadioButton rdbtn_time = new JRadioButton("시간 정렬");
		rdbtn_time.setToolTipText("빠른 시간 기준입니다.");
		rdbtn_time.setSelected(true);
		bgRS_Radio.add(rdbtn_time);
		rdbtn_time.setBounds(617, 33, 80, 23);
		//rdbtn_time.addItemListener(new SelectItemListener());
		routeScanPanel.add(rdbtn_time);

		JRadioButton rdbtn_Distance = new JRadioButton("거리 정렬");
		rdbtn_Distance.setToolTipText("빠른 거리 기준 입니다.");
		bgRS_Radio.add(rdbtn_Distance);
		rdbtn_Distance.setBounds(617, 62, 80, 23);
		//rdbtn_Distance.addItemListener(new SelectItemListener());
		routeScanPanel.add(rdbtn_Distance);
		

		JScrollPane scrollPane_RouteScan = new JScrollPane();
		scrollPane_RouteScan.setEnabled(false);
		scrollPane_RouteScan.setBounds(12, 92, 701, 557);
		routeScanPanel.add(scrollPane_RouteScan);

		tb_RouteScan = new JTable();
		tb_RouteScan.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tb_RouteScan.setCellSelectionEnabled(true);
		tb_RouteScan.getTableHeader().setReorderingAllowed(false); // 컬럼 이동 불가
		tb_RouteScan.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.ORANGE, null, null, null));
		scrollPane_RouteScan.setViewportView(tb_RouteScan);

//		tb_RouteScanModel = new DefaultTableModel(ResourceData.VECTOR_JTABLE_ROUTE_SCAN_COLUMNS, 0);
		tb_RouteScan.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"\uC21C\uC11C", "\uC81C\uBAA9", "\uC8FC\uC18C", "\uC704\uB3C4", "\uACBD\uB3C4"
			}
		));
		tb_RouteScan.getColumnModel().getColumn(0).setPreferredWidth(37);
		tb_RouteScan.getColumnModel().getColumn(1).setPreferredWidth(217);
		tb_RouteScan.getColumnModel().getColumn(2).setPreferredWidth(251);
		tb_RouteScan.getColumnModel().getColumn(3).setPreferredWidth(100);
		tb_RouteScan.getColumnModel().getColumn(4).setPreferredWidth(100);
		JButton btnExecute = new JButton("경로 탐색");
		btnExecute.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		//btnExecute.addActionListener(new EventActionListener(this));
		btnExecute.setToolTipText("\uBCC0\uD658");
		btnExecute.setBounds(495, 6, 110, 23);
		routeScanPanel.add(btnExecute);
		
				JButton btnRS_Delete = new JButton("검색 삭제");
				btnRS_Delete.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					}
				});
				btnRS_Delete.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
				btnRS_Delete.setToolTipText("마지막에 입력된 내용을 지웁니다.");
				btnRS_Delete.setBounds(374, 61, 110, 23);
				//btnRS_Delete.addActionListener(new EventActionListener(this));
				routeScanPanel.add(btnRS_Delete);
		
				JButton btnRS_Search = new JButton("주소 검색");
				btnRS_Search.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
				btnRS_Search.setToolTipText("입력된 주소를 기준으로 좌표를 검색합니다.");
				btnRS_Search.setBounds(374, 6, 110, 23);
				//btnRS_Search.addActionListener(new EventActionListener(this));
				routeScanPanel.add(btnRS_Search);
		
		JButton button = new JButton("등록");
		button.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		button.setToolTipText("입력된 주소를 기준으로 좌표를 검색합니다.");
		button.setBounds(374, 33, 110, 23);
		routeScanPanel.add(button);
		
		JButton button_1 = new JButton("전체 삭제");
		button_1.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_1.setToolTipText("데이터 전체 삭제를 합니다.");
		button_1.setBounds(495, 61, 110, 23);
		routeScanPanel.add(button_1);
		
		tf_searchDelete = new JTextField();
		tf_searchDelete.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tf_searchDelete.setToolTipText("지도의 출력의 지역을 입력하세요.");
		tf_searchDelete.setColumns(10);
		tf_searchDelete.setBounds(94, 62, 268, 21);
		routeScanPanel.add(tf_searchDelete);

		tfRS_Address = new JTextField();
		tfRS_Address.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tfRS_Address.setToolTipText("주소를 입력 하세요");
		tfRS_Address.setBounds(94, 7, 268, 21);
		routeScanPanel.add(tfRS_Address);
		tfRS_Address.setColumns(10);

		tfRS_Title = new JTextField();
		tfRS_Title.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tfRS_Title.setToolTipText("제목을 입력하세요");
		tfRS_Title.setColumns(10);
		tfRS_Title.setBounds(94, 35, 268, 21);
		routeScanPanel.add(tfRS_Title);
		
		tfRS_Location = new JTextField();
		tfRS_Location.setToolTipText("지도의 출력의 지역을 입력하세요.");
		tfRS_Location.setColumns(10);
		tfRS_Location.setBounds(818, 8, 198, 21);
		routeScanPanel.add(tfRS_Location);
		
		JLabel lb_address = new JLabel("주소 입력");
		lb_address.setBounds(25, 10, 57, 15);
		routeScanPanel.add(lb_address);
		
		JLabel lb_title = new JLabel("제목 입력");
		lb_title.setBounds(25, 38, 57, 15);
		routeScanPanel.add(lb_title);
				
		JLabel lb_sort = new JLabel("정렬 순서");
		lb_sort.setBounds(629, 11, 57, 15);
		routeScanPanel.add(lb_sort);
				
		JLabel lb_searchDelete = new JLabel("검색 삭제");
		lb_searchDelete.setBounds(25, 67, 57, 15);
		routeScanPanel.add(lb_searchDelete);
				
		JLabel lb_sigungu = new JLabel("시, 군, 구");
		lb_sigungu.setBounds(749, 11, 57, 15);
		routeScanPanel.add(lb_sigungu);
		
		lbRS_ImgIcon = new JLabel();
		lbRS_ImgIcon.setForeground(Color.GRAY);
		lbRS_ImgIcon.setBackground(new Color(0, 100, 0));
		lbRS_ImgIcon.setBounds(725, 43, 492, 606);
		routeScanPanel.add(lbRS_ImgIcon);
		
		JLabel label = new JLabel("");
		label.setBounds(527, 33, 583, 544);
		routeScanPanel.add(label);
		
		tfRS_LimitCnt = new JTextField();
		tfRS_LimitCnt.setToolTipText("총 페이지 수");
		tfRS_LimitCnt.setEditable(false);
		tfRS_LimitCnt.setColumns(10);
		tfRS_LimitCnt.setBounds(495, 34, 110, 23);
		routeScanPanel.add(tfRS_LimitCnt);
		
		tfRS_Zoom = new JTextField();
		tfRS_Zoom.setToolTipText("지도의 출력의 지역을 입력하세요.");
		tfRS_Zoom.setColumns(10);
		tfRS_Zoom.setBounds(1100, 8, 117, 21);
		routeScanPanel.add(tfRS_Zoom);
		
		JLabel lbRS_Zoom = new JLabel("줌 배율");
		lbRS_Zoom.setBounds(1050, 11, 45, 15);
		routeScanPanel.add(lbRS_Zoom);
		
		JPanel geoCodepanel = new JPanel();
		tabbedPane.addTab("GeoCode", null, geoCodepanel, null);
		geoCodepanel.setLayout(null);

		tfGC_Address = new JTextField();
		tfGC_Address.setToolTipText("주소를 입력하세요.");
		tfGC_Address.setBounds(109, 10, 492, 21);
		geoCodepanel.add(tfGC_Address);
		tfGC_Address.setColumns(10);

		JButton btnGC_Search = new JButton("조회");
		btnGC_Search.setToolTipText("주소의 좌표를 검색 합니다.");
		btnGC_Search.setBounds(504, 72, 97, 23);
		//btnGC_Search.addActionListener(new EventActionListener(this));
		geoCodepanel.add(btnGC_Search);

		tfGC_GeoCode = new JTextField();
		tfGC_GeoCode.setToolTipText("검색 결과 좌표를 출력 합니다.");
		tfGC_GeoCode.setBounds(109, 41, 492, 21);
		geoCodepanel.add(tfGC_GeoCode);
		tfGC_GeoCode.setColumns(10);

		JLabel lb_Address = new JLabel("주소 입력");
		lb_Address.setBounds(40, 13, 57, 15);
		geoCodepanel.add(lb_Address);

		JLabel lbGC_GeoCode = new JLabel("출력 좌표");
		lbGC_GeoCode.setBounds(40, 44, 57, 15);
		geoCodepanel.add(lbGC_GeoCode);

		lbGC_ImgIcon = new JLabel("");
		lbGC_ImgIcon.setBounds(12, 105, 583, 544);
		geoCodepanel.add(lbGC_ImgIcon);
		
		Canvas canvas = new Canvas();
		canvas.setBackground(Color.BLACK);
		canvas.setBounds(607, 10, 1, 639);
		geoCodepanel.add(canvas);
		
		JRadioButton rdbtn_All = new JRadioButton("\uC804\uCCB4 \uBCC0\uD658");
		rdbtn_All.setBounds(660, 8, 104, 23);
		geoCodepanel.add(rdbtn_All);
		bgPT_Radio.add(rdbtn_All);
		
		JRadioButton rdbtn_Partial = new JRadioButton("\uBD80\uBD84 \uBCC0\uD658");
		rdbtn_Partial.setBounds(775, 8, 104, 23);
		geoCodepanel.add(rdbtn_Partial);
		rdbtn_Partial.setSelected(true);
		bgPT_Radio.add(rdbtn_Partial);
		
		tfPT_PageNum = new JTextField();
		tfPT_PageNum.setBounds(741, 73, 441, 21);
		geoCodepanel.add(tfPT_PageNum);
		tfPT_PageNum.setToolTipText("변환 할 페이지 번호를 입력하세요. 없을 경우 전체 변환 됩니다.");
		tfPT_PageNum.setColumns(10);
		
		tfPT_DocPw = new JTextField();
		tfPT_DocPw.setBounds(741, 105, 441, 21);
		geoCodepanel.add(tfPT_DocPw);
		tfPT_DocPw.setToolTipText("문서 비밀번호가 있을 경우 입력하세요");
		tfPT_DocPw.setColumns(10);
		
		JLabel lb_totPageNum = new JLabel("총 페이지 수");
		lb_totPageNum.setBounds(660, 44, 75, 15);
		geoCodepanel.add(lb_totPageNum);
		
		JLabel lb_PageNum = new JLabel("페이지 번호");
		lb_PageNum.setBounds(660, 76, 75, 15);
		geoCodepanel.add(lb_PageNum);
		
		JLabel lb_DocPw = new JLabel("문서 비밀번호");
		lb_DocPw.setBounds(660, 108, 80, 15);
		geoCodepanel.add(lb_DocPw);
		
		JLabel lb_ImgResolution = new JLabel("이미지 품질");
		lb_ImgResolution.setBounds(660, 140, 80, 15);
		geoCodepanel.add(lb_ImgResolution);
		
		tfPT_Resolution = new JTextField();
		tfPT_Resolution.setBounds(741, 137, 441, 21);
		geoCodepanel.add(tfPT_Resolution);
		tfPT_Resolution.setToolTipText("출력 해상도");
		tfPT_Resolution.setColumns(10);
		
		JButton btnBT_Transfer = new JButton("변환");
		btnBT_Transfer.setBounds(1085, 168, 97, 23);
		geoCodepanel.add(btnBT_Transfer);
		btnBT_Transfer.setToolTipText("pdf 를 이미지로 변환 합니다.");
		
		tfPT_TotPageNum = new JTextField();
		tfPT_TotPageNum.setBounds(741, 41, 441, 21);
		geoCodepanel.add(tfPT_TotPageNum);
		tfPT_TotPageNum.setEditable(false);
		tfPT_TotPageNum.setToolTipText("총 페이지 수");
		tfPT_TotPageNum.setColumns(10);
		
		tfPT_HiddenFilePath = new JTextField(){
			public void setBorder(Border border){ } };
		tfPT_HiddenFilePath.setBounds(717, 104, 0, 0);
		geoCodepanel.add(tfPT_HiddenFilePath);
		tfPT_HiddenFilePath.setEnabled(false);
		tfPT_HiddenFilePath.setEditable(false);
		tfPT_HiddenFilePath.setColumns(0);

		JPanel bottom_panel = new JPanel();
		contentPane.add(bottom_panel, BorderLayout.SOUTH);

		btnBT_Directory = new JButton("Directory");
		//btnBT_Directory.addActionListener(new EventActionListener(this));
		bottom_panel.add(btnBT_Directory);

		btnBT_Files = new JButton("Files");
		//btnBT_Files.addActionListener(new EventActionListener(this));
		bottom_panel.add(btnBT_Files);

		btnBT_Save = new JButton("Save");
		//btnBT_Save.addActionListener(new EventActionListener(this));
		bottom_panel.add(btnBT_Save);

		btnBT_Clear = new JButton("Clear");
		//btnBT_Clear.addActionListener(new EventActionListener(this));
		bottom_panel.add(btnBT_Clear);
		
		
	}
}
