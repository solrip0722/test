

import java.util.Comparator;

public class RouteDistanceCompare implements Comparator<GeoData> {
	/**
	 * 최단거리
	 */
	public int compare(GeoData arg0, GeoData arg1) {
		// TODO Auto-generated method stub
		return arg0.getTotalDistance().compareTo(arg1.getTotalDistance());
	}

}
