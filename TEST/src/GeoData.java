public class GeoData {

	private String startTitle;
	private String endTitle;
	private Integer totalDistance;
	private Integer totalTime;
	private String startX;
	private String startY;
	private String endX;
	private String endY;
	private String startAddress;
	private String endAddress;

	public String getStartTitle() {
		return startTitle;
	}

	public void setStartTitle(String startTitle) {
		this.startTitle = startTitle;
	}

	public String getEndTitle() {
		return endTitle;
	}

	public void setEndTitle(String endTitle) {
		this.endTitle = endTitle;
	}

	public Integer getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(Integer totalDistance) {
		this.totalDistance = totalDistance;
	}

	public Integer getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(Integer totalTime) {
		this.totalTime = totalTime;
	}

	public String getStartX() {
		return startX;
	}

	public void setStartX(String startX) {
		this.startX = startX;
	}

	public String getStartY() {
		return startY;
	}

	public void setStartY(String startY) {
		this.startY = startY;
	}

	public String getEndX() {
		return endX;
	}

	public void setEndX(String endX) {
		this.endX = endX;
	}

	public String getEndY() {
		return endY;
	}

	public void setEndY(String endY) {
		this.endY = endY;
	}

	public String getStartAddress() {
		return startAddress;
	}

	public void setStartAddress(String startAddress) {
		this.startAddress = startAddress;
	}

	public String getEndAddress() {
		return endAddress;
	}

	public void setEndAddress(String endAddress) {
		this.endAddress = endAddress;
	}

	@Override
	public String toString() {
		return "totalDistance=" + totalDistance + ", totalTime=" + totalTime + ", startAddress=" + startAddress
				+ ", endAddress=" + endAddress;
	}

}
	