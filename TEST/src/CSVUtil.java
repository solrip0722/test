import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Vector;

public class CSVUtil {

	public static void writeCSV(Vector<String> dataVector) {
		
		// 현재 인코딩을 확인한다.
		// String enc = new OutputStreamWriter(System.out).getEncoding();
		BufferedWriter bufferedWriter = null;
		try {

			/*
			 * FileWriter로 쓸 경우 csv에 글씨가 깨져서 나오므로 BufferedWriter를 이용해서   MS949 캐릭터 셋으로 쓴다.
			 */
			String dataInfo = null;
			String str_ExportFilePath = "D://01.solrip0722//01.eclipse//01.workspace//TEST//data" + File.separator + "fastRoot.csv";
			bufferedWriter = new BufferedWriter(
					new OutputStreamWriter(
					new FileOutputStream(str_ExportFilePath), "ms949"));
			while (true)
			{
				if ( dataVector != null && dataVector.size() > 0)
				{
					dataInfo = dataVector.remove(0);
					bufferedWriter.write(dataInfo);
				} else {
					System.out.println("csv write finish !!");
					bufferedWriter.close();
					return;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try { if (bufferedWriter != null)bufferedWriter.close(); } catch (IOException e) { }
		}
	}
}