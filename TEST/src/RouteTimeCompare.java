import java.util.Comparator;

public class RouteTimeCompare implements Comparator<GeoData> {
	 
	/**
	 * 최단시간
	 */
	public int compare(GeoData arg0, GeoData arg1) {
		// TODO Auto-generated method stub
		return arg0.getTotalTime().compareTo(arg1.getTotalTime());
	}
}
