
public class TimeUtil {
	public static String getMilSec(long milSec) {
		StringBuffer sb = new StringBuffer();
		int resultVal = (int) (milSec);

		int resultsec = (resultVal / 1000) % (60);
		int resultmin = (resultVal / (1000 * 60)) % 60;
		int resultHour = (resultVal / (1000 * 60 * 60)) % 24;
		int resultday = (resultVal / (1000 * 60 * 60)) / 24;
		
		if (resultHour > 0)
			sb.append(resultHour).append("시간 ");
		if (resultmin > 0)
			sb.append(resultmin).append("분 ");
		if (resultday > 0)
			sb.append(resultday).append("일 ");
		sb.append(resultsec).append("초");
		
		return sb.toString();
	}
	
	public static void main(String[] args) {
//		System.out.println(getMilSec(18261000));
		System.out.println(getMilSec(1108));
	}

}
